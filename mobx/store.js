import { action, extendObservable } from 'mobx';


class AppStore {

	constructor(){
		extendObservable(this, {
			locationForm: {
				name: "",
				location: "",
				numStalls: "",
			}
		})		
	}

	setName(name){
		this.locationForm.name = name;
	}

	setLocation(location){
		this.locationForm.location = location;
	}

	setNumStalls(numStalls){
		this.locationForm.numStalls = numStalls;
	}

}

const appStore = new AppStore()
export default appStore