/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Button,
  FlatList,
  View,
  ScrollView,
  TouchableHighlight,
} from 'react-native';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import { StackNavigator } from 'react-navigation';
import { Col, Row, Grid } from "react-native-easy-grid";

import {
  getTheme,
  MKTextField,
  MKColor,
} from 'react-native-material-kit';

import {observer} from 'mobx-react/native'
import appStore from './mobx/store';

// const firebase = RNFirebase.initializeApp({
//   debug: true,
// });

const theme = getTheme();

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  text: {
    textAlign: 'center',
  },
  locationTitle: {
    fontSize: 16,
    fontWeight: "bold",
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    width: '100%',
  },
  locationItem: {
    borderWidth: 0.4,
    width: '100%',
    padding: 25,
  },
  locationAddress: {
    fontSize: 12,
    fontStyle: 'italic',
  },
  textfield: {
    padding: 25,
  },
});

var config = {
  apiKey: "AIzaSyBx0QTWgb6QqJje_MoaQinB4NxNahr-9kw",
  authDomain: "sapient-biplane-521.firebaseapp.com",
  databaseURL: "https://sapient-biplane-521.firebaseio.com/",
  // storageBucket: "bucket.appspot.com"
};
firebase.initializeApp(config);

// Get a reference to the database service
var database = firebase.database();


// initialize mobx store
class AddLocationScreen extends React.Component {

  constructor(props) {
    super(props)
    const { params } = this.props.navigation.state;
    this.setStoreProp = this.setStoreProp.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state
    return {
      title: 'Add Location',
      // {this.handleLocationSave} is no good because this is static.
      headerRight: <Button title="Save" onPress={() => params.handleLocationSave(navigation)} />
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({ handleLocationSave: this.handleLocationSave });
  }

  handleLocationSave(navigation) {
    // ADD FIREBASE CODE

    database.ref('users/golden-state-parking/locations/' + appStore.locationForm.name).set({
      name: appStore.locationForm.name,
      location: appStore.locationForm.location,
      numStalls: appStore.locationForm.numStalls,
    });

    navigation.navigate('Home')

  }

  setStoreProp(type, text){
    switch(type){
      case "name":
        this.props.screenProps.setName(text);
      break;
      case "location":
        this.props.screenProps.setLocation(text);
      case "numStalls":
        this.props.screenProps.setNumStalls(text);
      default:
      break;
    }
  }

  render() {
    const { locationForm } = this.props.screenProps;

    return (
      <View>
        <MKTextField
          placeholder="Enter name"
          onChangeText={(text) => this.setStoreProp('name', text)}
          style={styles.textfield}
        />
        <MKTextField
          placeholder="Enter street address"
          onChangeText={(text) => this.setStoreProp('location', text)}
          style={styles.textfield}
        />
        <MKTextField
          placeholder="Enter number of parking stalls"
          onChangeText={(text) => this.setStoreProp('numStalls', text)}
          style={styles.textfield}
        />
      </View>
    )
  }
}


class ValetItem extends React.PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.id);
  }

  render() {
    return (
      <View style={[theme.cardContent]}>
        <TouchableHighlight>
          <Grid>
            <Col size={70}>
              <Row>
                <Text style={styles.locationTitle}>
                  { this.props.name }
                </Text>
              </Row>
            </Col>
            <Col size={30}>
              <Text>
                100 Tickets Scanned
              </Text>
            </Col>
          </Grid>
        </TouchableHighlight>
      </View>
    );
  }
}

class ValetList extends React.Component {

  _renderItem = ({ item }) => (
    <ValetItem
      id={item.id}
      name={item.name}
    />
  )

  render() {
    return (
      <View>
        <FlatList
          data={this.props.data}
          extraData={this.state}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

class ValetScreen extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      data: [],
    }
  }
   static navigationOptions = ({ navigation }) => ({
    title: 'My Valets',
  })   

  render() {

    var data = []

    var locationRef = firebase.database().ref('users/golden-state-parking/valets');
    locationRef.once('value').then(function(snapshot){
      this.state.data.push(snapshot.val())
    });

    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={theme.cardStyle}>
            <ValetList data={this.state.data} />
          </View>
        </View>
      </ScrollView>
    );
  } 
}

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'My Locations',
    headerLeft: <Button title="Lock" onPress={() => navigation.navigate('ValetScreen')} />,
    headerRight: <Button title="Add" onPress={() => navigation.navigate('AddLocation')} />,
  })

  constructor(props){
    super(props)
    this.state = {
      data: [],
    }
  }

  componentWillMount(){
    var that = this
    var locationRef = firebase.database().ref('users/golden-state-parking/locations');
    var data = []
    locationRef.once('value')
    .then(function(snapshot){
      var obj = snapshot.val()
      var keys = Object.keys(obj)
      keys.forEach(function(key){
        data.push({ title: obj[key].name, id: key, address: obj[key].location, numStalls: obj[key].numStalls })
      })
    })
    .then(function(){
      that.setState({ data: data })
    })

  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={theme.cardStyle}>
            <LocationsList data={this.state.data} />
          </View>
        </View>
      </ScrollView>
    );
  }
}

class LocationItem extends React.PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.id);
  }

  render() {
    return (
      <View style={[theme.cardContent, {padding: 25 }]}>
        <TouchableHighlight>
          <Grid>
            <Col size={70}>
              <Row>
                <Text style={styles.locationTitle}>
                  { this.props.title }
                </Text>
              </Row>
              <Row>
                <Text style={styles.locationAddress}>
                  { this.props.address }
                </Text>
              </Row>
            </Col>
            <Col size={30}>
              <Text>
                {this.props.numStalls}
              </Text>
            </Col>
          </Grid>
        </TouchableHighlight>
      </View>
    );
  }
}

class LocationsList extends React.Component {
  state = {};

  _renderItem = ({ item }) => (
    <LocationItem
      id={item.id}
      title={item.title}
      address={item.address}
      numStalls={item.numStalls}
    />
  )

  render() {
    return (
      <View>
        <FlatList
          data={this.props.data}
          extraData={this.state}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

@observer
class MyApp extends React.Component {
  render() {
    console.log(appStore)

    // StackNavigator **only** accepts a screenProps prop so we're passing
    // initialProps through that.
    return <MainModalNavigator screenProps={appStore} />; 
  }
}


const MainCardNavigator = StackNavigator(
  {
    Home: { screen: HomeScreen },
    AddLocation: { screen: AddLocationScreen },
  },
);

const MainModalNavigator = StackNavigator(
  {
    MainCardNavigator: { screen: MainCardNavigator },
    ValetScreen: { screen: ValetScreen },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);


AppRegistry.registerComponent('test', () => MyApp);
